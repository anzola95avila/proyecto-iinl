#ifndef LOGGER_CPP
#define LOGGER_CPP

#include <Arduino.h>
#include <stdio.h>

#define ERROR 0
#define INFO 1
#define DEBUG 2

class logger {
public:
    logger(int lvl = 2) {
	level = max(0, lvl % 3);
    }
    
    void info(const char *);
    void debug(const char *);
    void error(const char *);
    
private:
    int level;
};

void logger::info(const char * msg) {
    if (level >= INFO) {
	Serial.print("[INFO ");
	Serial.print(millis());
	Serial.print("] ");
	Serial.println(msg);
    }
}

void logger::debug(const char * msg) {
    if (level >= DEBUG) {
	Serial.print("[DEBUG ");
	Serial.print(millis());
	Serial.print("] ");
	Serial.println(msg);
    }
}

void logger::error(const char * msg) {
    Serial.print("[ERROR ");
    Serial.print(millis());
    Serial.print("] ");
    Serial.println(msg);
}

#endif
