#include "robot/robot.hpp"
#include "robot/robot_mbot.cpp"
#include "robot/robot_logic.cpp"
#include "robot/logger.cpp"
#include "MeMCore.h"

#define ULTRA PORT_3
#define LINE PORT_2
#define MOTOR1 M1
#define MOTOR2 M2
#define BOARD_BUTTON A7
#define UNITS 1000

MeLineFollower lineFinder(LINE);
MeDCMotor motor1(M1);
MeDCMotor motor2(M2);
MeUltrasonicSensor ultraSensor(ULTRA);
MeRGBLed rgbleds(7, 2);

robot *mBot;
rlogic *logic;
logger *logg;

typedef long ll;

time_t current = 0;
time_t previous = 0;

void setup() {
  pinMode(BOARD_BUTTON, INPUT);
  Serial.begin(9600);
  mBot = new mbot(&ultraSensor,
         &lineFinder,
         &motor1,
         &motor2,
         &rgbleds,
         BOARD_BUTTON);
  logg = new logger(2);
  logic = new rlogic(mBot, logg, UNITS);
  logg->info("Se inicia mBot");
}

void loop() {
  current = millis();
  logic->update(abs(current - previous));
  previous = current;
  delay(10);
  current = millis();
}
